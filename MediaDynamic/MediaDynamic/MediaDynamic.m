//
//  MediaDynamic.m
//  MediaDynamic
//
//  Created by Prashant Durgavajjala on 14/03/2020.
//  Copyright © 2020 Prashant Durgavajjala. All rights reserved.
//

#import "MediaDynamic.h"
#import "MediaSorting.h"

#define MAX_MOVIES_COUNT 10 //Maximum count of movies to display

@interface MediaDynamic()

@property (nonatomic, assign) NSInteger * totalPages; // Total Pages for the data to be fetched
@property (nonatomic, strong) NSMutableArray * titleArray; // Array containing all the movie titles
@property (nonatomic, strong) NSMutableArray * ratingArray; // Array containing all the ratings

@end

@implementation MediaDynamic

- (void)ReceiveData
{
    //Call the function to fetch data
    [self UrlConnect];
}

//Fetch data for the given page
- (void)requestComment:(NSUInteger)comment
             completed:(void(^)(id))completed
{
    //Fetch api key
    NSString * apiKey = [[NSUserDefaults standardUserDefaults] objectForKey:@"APIKEY"];
    //Decrypt api key
    NSString * decodedKey=[[NSString alloc]initWithData:apiKey encoding:NSWindowsCP1251StringEncoding];
    
    if(apiKey.length <=0)
        return;
    
    //Construct the link for api call
    NSString * query = @"";
    query = [query stringByAppendingString:@"https://api.themoviedb.org/3/search/movie?api_key="];
    query = [query stringByAppendingString:decodedKey];
    query = [query stringByAppendingString:[NSString stringWithFormat:@"&page=%lu&query=", (unsigned long)comment]];
    NSString * queryedit = [query stringByAppendingString:self.textEntered];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:queryedit]
                                            completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                                //Parsing api response
                                                NSError *jsonError;
                                                NSArray *parsedJSONArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                                                
                                                //Fetch all the movies with movie name entered
                                                NSString * resultConditions = [parsedJSONArray description];
                                                NSArray *results = [parsedJSONArray valueForKey:@"results"];
                                                for(int i=0;i<[results count];i++)
                                                {
                                                    NSDictionary *dict1 = [results objectAtIndex:i];
                                                    //Collect only 2017 and 2018 movie details
                                                    NSString * releaseDate = [dict1 valueForKey:@"release_date"];
                                                    if(![releaseDate isEqualToString:@""])
                                                    {
                                                        //Substring for year
                                                        NSString * temp = [releaseDate substringWithRange:NSMakeRange(0, 4)];
                                                        
                                                        //Store titles and rating details
                                                        if ([temp isEqualToString:@"2017"] || [temp isEqualToString:@"2018"]) {
                                                            [self.ratingArray addObject:[dict1 valueForKey:@"vote_average"]];
                                                            [self.titleArray addObject:[dict1 valueForKey:@"original_title"]];
                                                        }
                                                    }
                                                }
                                                completed(parsedJSONArray);    }];
    [dataTask resume];
    
}

//Callback function when all the pages details have been fetched
- (void)completion{
    
    //Data to be sent to C static library for sorting
    int size = (int) self.ratingArray.count;
    float  ratings[size];
    char * titles[size];
    
    for(int i=0; i<size; i++)
    {
        ratings[i] = [[self.ratingArray objectAtIndex:i] floatValue];
        titles[i] = [[self.titleArray objectAtIndex:i] UTF8String];
    }
    sortArrays(ratings, titles, size); //SOrt function in C static library to sort titles based on ratings
    
    //Result data string is formed
    NSString * result = @"";
    for (int i = 0; i < size; i++)
    {
        //Check for maximum movies display limit
        if(i>=MAX_MOVIES_COUNT)
            break;
        
        NSString * titlestr = [NSString stringWithUTF8String:titles[i]];
        NSString * ratingstr = [NSString stringWithFormat:@"       %.2f ",ratings[i]];
        result = [result stringByAppendingString:titlestr];
        result = [result stringByAppendingString:ratingstr];
        result = [result stringByAppendingString:@" \r "];
    }
    //Send call back to UI View Controller for final result
    [[NSNotificationCenter defaultCenter] postNotificationName:@"com.media.notification" object:result];
}

//Call back function after first call to server. Based on total pages, collect all the movie titles from all the pages
- (void)callBackFunction{
    //Wait for data to be fetched from all the pages
    dispatch_group_t group = dispatch_group_create();
    for (NSUInteger i = 2; i <= self.totalPages; i++) //Loop for collecting each page data
    {
        dispatch_group_enter(group);
        [self requestComment:i
                   completed:^(id response){
                       dispatch_group_leave(group);
                   }];
    }
    dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
    [self completion];
}

//Create urlsession and make rest call for first page
- (void)UrlConnect
{
    //Fetch api key and decode
    NSString *apiKey = [[NSUserDefaults standardUserDefaults] objectForKey:@"APIKEY"];
    NSString *decodedKey=[[NSString alloc]initWithData:apiKey encoding:NSWindowsCP1251StringEncoding];
    
    if(apiKey.length <=0)
        return;
    
    //Construct url for first page
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString * query = @"";
    query = [query stringByAppendingString:@"https://api.themoviedb.org/3/search/movie?api_key="];
    query = [query stringByAppendingString:decodedKey];
    query = [query stringByAppendingString:@"&query="];
    NSString * queryedit = [query stringByAppendingString:self.textEntered];
    
    // insert whatever URL you would like to connect to
    [request setURL:[NSURL URLWithString:queryedit]];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^( NSData *data, NSURLResponse *response, NSError *error )
                 {
                     dispatch_async( dispatch_get_main_queue(),
                                    ^{
                                        // parse returned JSON array
                                        NSError *jsonError;
                                        NSArray *parsedJSONArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                                        
                                        //Read total pages and information in first page
                                        NSString * resultConditions = [parsedJSONArray description];
                                        self.totalPages = [[parsedJSONArray valueForKey:@"total_pages"] intValue];
                                        
                                        NSArray *results = [parsedJSONArray valueForKey:@"results"];
                                        self.titleArray = [[NSMutableArray alloc] init];
                                        self.ratingArray = [[NSMutableArray alloc] init];
                                        
                                        for(int i=0;i<[results count];i++)
                                        {
                                            NSDictionary *dict1 = [results objectAtIndex:i];
                                            
                                            NSString * releaseDate = [dict1 valueForKey:@"release_date"];
                                            
                                            if(![releaseDate isEqualToString:@""])
                                            {
                                                //Substring for year
                                                NSString * temp = [releaseDate substringWithRange:NSMakeRange(0, 4)];
                                                
                                                //Collect only 2017 and 2018 movies
                                                if ([temp isEqualToString:@"2017"] || [temp isEqualToString:@"2018"]) {
                                                    [self.ratingArray addObject:[dict1 valueForKey:@"vote_average"]];
                                                    [self.titleArray addObject:[dict1 valueForKey:@"original_title"]];
                                                }
                                            }
                                        }
                                        [self callBackFunction];    } );   }];
    [dataTask resume];
}

@end
