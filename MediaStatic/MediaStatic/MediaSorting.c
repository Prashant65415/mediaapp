//
//  MediaSorting.c
//  MediaStatic
//
//  Created by Prashant Durgavajjala on 14/03/2020.
//  Copyright © 2020 Prashant Durgavajjala. All rights reserved.
//

#include "MediaSorting.h"

typedef int bool;
#define true 1
#define false 0

//Swap Strings
void swapStrings(char **str1_ptr, char **str2_ptr)
{
    char *temp = *str1_ptr;
    *str1_ptr = *str2_ptr;
    *str2_ptr = temp;
}

//Swap float values
void swapValues(float *xp, float *yp)
{
    float temp = *xp;
    *xp = *yp;
    *yp = temp;
}

//Bubble sort algorithm
void sortArrays(float * ratings,  char ** titles, int size)
{
    int i, j;
    bool swapped;
    for (i = 0; i < size-1; i++)
    {
        swapped = false;
        for (j = 0; j < size-i-1; j++)
        {
            //Based on ratings, sort both ratings and titles
            if (ratings[j] < ratings[j+1])
            {
                swapValues(&ratings[j], &ratings[j+1]);
                swapStrings(&titles[j], &titles[j+1]);
                swapped = true;
            }
        }
        
        // IF no two elements were swapped by inner loop, then break
        if (swapped == false)
            break;
    }
}
