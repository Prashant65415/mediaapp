//
//  MediaAppTests.m
//  MediaAppTests
//
//  Created by Prashant Durgavajjala on 12/03/2020.
//  Copyright © 2020 Prashant Durgavajjala. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MediaDynamic.h"
#import "MediaSorting.h"

@interface MediaAppTests : XCTestCase

@property (strong, nonatomic) MediaDynamic * mediadynamic;

@end

@implementation MediaAppTests

- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testDynamicLib {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadData:) name:@"com.media.notification" object:nil];
    self.mediadynamic = [MediaDynamic new];
    
    NSString *textEnter = @"Thor";
    self.mediadynamic.textEntered = textEnter;
    [self.mediadynamic ReceiveData];
}

-(void)loadData:(NSNotification *)notif {
    NSString *result = [notif object];
    
    XCTAssertNotEqualObjects(result, nil, "Testing result is not nul");
}

- (void)testStaticLib {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    NSMutableArray * resultArray;
    NSMutableArray * ratingsArray;
    float result[3] = {1.0f, 2.0f, 7.0f};
    int size = 3;
    float ratings[3] = {2.0f, 7.0f, 1.0f};
    char * titles[3] = {"Thor2", "Thor3", "Thor"};
    sortArrays(ratings, titles, size);
    
    for (int i = 0; i < size; i++) {
        NSNumber *number = [NSNumber numberWithFloat:result[i]];
        [resultArray addObject:number];
    }
    
    for (int i = 0; i < size; i++) {
        NSNumber *number = [NSNumber numberWithFloat:ratings[i]];
        [ratingsArray addObject:number];
    }
    
    XCTAssertEqualObjects(ratingsArray, resultArray, "Test sorting function");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
