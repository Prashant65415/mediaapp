//
//  MediaSorting.h
//  MediaStatic
//
//  Created by Prashant Durgavajjala on 14/03/2020.
//  Copyright © 2020 Prashant Durgavajjala. All rights reserved.
//

#ifndef MediaSorting_h
#define MediaSorting_h

#include <stdio.h>

//Swap strings
void swapStrings(char **str1_ptr, char **str2_ptr);

//Swap flat values
void swapValues(float *xp, float *yp);

//Bubble sort algorithm
void sortArrays(float * ratings,  char ** titles, int size);

#endif /* MediaSorting_h */
