//
//  AppDelegate.h
//  MediaApp
//
//  Created by Prashant Durgavajjala on 12/03/2020.
//  Copyright © 2020 Prashant Durgavajjala. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

