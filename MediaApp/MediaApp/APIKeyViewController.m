//
//  ApikeyViewController.m
//  MediaApp
//
//  Created by Prashant Durgavajjala on 14/03/2020.
//  Copyright © 2020 Prashant Durgavajjala. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIKeyViewController.h"

@interface APIKeyViewController ()

@property (weak, nonatomic) IBOutlet UITextField *apiKeyString;

@end

@implementation APIKeyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Capture button pressed after enetering api key
- (IBAction)captureAPIKey:(id)sender {
    NSString * apiKeyStr = self.apiKeyString.text;
    if(apiKeyStr.length>0)
    {
        //Encrypt api key and store in userdefaults
        NSData *data=[apiKeyStr dataUsingEncoding:NSWindowsCP1251StringEncoding];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setObject: data forKey:@"APIKEY"];
        [prefs synchronize];
    }
    
    //Transition to Main Screen from api key entery screen
    [self performSegueWithIdentifier:@"showMainScreen" sender:self];
}

@end

