//
//  ViewController.m
//  MediaApp
//
//  Created by Prashant Durgavajjala on 12/03/2020.
//  Copyright © 2020 Prashant Durgavajjala. All rights reserved.
//

#import "ViewController.h"
#import "MediaSorting.h"
#import "MediaDynamic.h"
#include <dlfcn.h>

@interface ViewController ()

@property (strong, nonatomic) MediaDynamic * mediadynamic;
@property (nonatomic,retain) NSString *userID;

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UITextField *textfield;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //Create NSObject instance from dynamic library
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadData:) name:@"com.media.notification" object:nil];
    self.mediadynamic = [MediaDynamic new];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//Submit button is pressed
- (IBAction)OnSubmit:(id)sender {
    NSString *textEnter = self.textfield.text;
    //send text entered to dynamic library NSObject
    self.mediadynamic.textEntered = textEnter;
    //Call receive data function to fetch data
    [self.mediadynamic ReceiveData];
}

//This function is called after the dynamic library has received the data
-(void)loadData:(NSNotification *)notif {
    NSString *result = [notif object];
    [self.textView setText:result]; // set received result to text view
}

@end
