//
//  MediaDynamic.h
//  MediaDynamic
//
//  Created by Prashant Durgavajjala on 14/03/2020.
//  Copyright © 2020 Prashant Durgavajjala. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MediaDynamic : NSObject

@property (nonatomic, assign) NSString * textEntered;

- (void)ReceiveData;

@end
