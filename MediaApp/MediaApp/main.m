//
//  main.m
//  MediaApp
//
//  Created by Prashant Durgavajjala on 12/03/2020.
//  Copyright © 2020 Prashant Durgavajjala. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
